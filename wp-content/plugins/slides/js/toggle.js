jQuery(document).ready(function() {

    var $page_template = $('.cmb_id_twpb_alert_test_textdate-2')
        ,$metabox = $('.cmb_id_twpb_alert_test_checkbox'); // For example

    $page_template.change(function() {
        if ($(this).val() == 'default') {
            $metabox.show();
        } else {
            $metabox.hide();
        }
    }).change();

});
