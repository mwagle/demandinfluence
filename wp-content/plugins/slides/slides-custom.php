<?php

function slide_custom_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Slides', 'Post Type General Name', 'outreach' ),
		'singular_name'       => _x( 'Slide', 'Post Type Singular Name', 'outreach' ),
		'menu_name'           => __( 'Slides', 'outreach' ),
		'parent_item_colon'   => __( 'Parent Slide', 'outreach' ),
		'all_items'           => __( 'All Slides', 'outreach' ),
		'view_item'           => __( 'View Slide', 'outreach' ),
		'add_new_item'        => __( 'Add New Slide', 'outreach' ),
		'add_new'             => __( 'Add New', 'outreach' ),
		'edit_item'           => __( 'Edit Slide', 'outreach' ),
		'update_item'         => __( 'Update Slide', 'outreach' ),
		'search_items'        => __( 'Search Slide', 'outreach' ),
		'not_found'           => __( 'Not Found', 'outreach' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'outreach' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'slides', 'outreach' ),
		'description'         => __( 'Slides', 'outreach' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
    //'rewrite'             => array( 'slug' => 'dolphin-events' ),
	);
	
	// Registering your Custom Post Type
	register_post_type( 'slides', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'slide_custom_post_type', 0 );


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_slides_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_slides_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels = array(
    'name' => _x( 'Slide Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Slide Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Slide Types' ),
    'all_items' => __( 'All Slide Types' ),
    'parent_item' => __( 'Parent Slide Type' ),
    'parent_item_colon' => __( 'Parent Slide Type:' ),
    'edit_item' => __( 'Edit Slide Type' ), 
    'update_item' => __( 'Update Slide Type' ),
    'add_new_item' => __( 'Add New Slide Type' ),
    'new_item_name' => __( 'New Slide type Name' ),
    'menu_name' => __( 'Slide types' ),
  ); 	

// Now register the taxonomy

  register_taxonomy('slidetypes',array('slides'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'slidetype' ),
  ));

}



