<?php
//Function for Home Top.

function home_bottom_callouts() {
?>
<div class="callouts col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="row">
    <div class="intention-based-leads callout col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '49',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <?php $single_img =  wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
      <a title="callout-more" href="<?php echo get_permalink(); ?>"><div class="callout-back one" style="background-image: url(<?php echo $single_img;?>)"></div></a>
      <h4><?php echo get_the_title(); ?></h4>

      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="lead-calculator callout col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '51',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <?php $single_img =  wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
      <a title="callout-more" href="<?php echo get_permalink(); ?>"><div class="callout-back two" style="background-image: url(<?php echo $single_img;?>)"></div></a>
      <h4><?php echo get_the_title(); ?></h4>

      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="appointment-settings callout col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '53',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <?php $single_img =  wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
      <a title="callout-more" href="<?php echo get_permalink(); ?>"><div class="callout-back three" style="background-image: url(<?php echo $single_img;?>)"></div></a>
      <h4><?php echo get_the_title(); ?></h4>

      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="account-watchdog callout col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '55',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <?php $single_img =  wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
      <a title="callout-more" href="<?php echo get_permalink(); ?>"><div class="callout-back four" style="background-image: url(<?php echo $single_img;?>)"></div></a>
      <h4><?php echo get_the_title(); ?></h4>

      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>
  </div>
</div>



<?php
}
add_shortcode( 'home-bottom-callouts', 'home_bottom_callouts' );










function testimonials_section() {
  ?>
  
        <!-- End Our courses titile -->
        <!-- Start Our courses content -->
        <div class="testimonial-holder col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="ourCourse_content">
              <ul class="testimonial_nav">
                
                   <?php
                  global $post;
                          $args = array(
                      'post_type' => 'testimonial',
                      'showposts' => -1,
                      'post_status' => 'publish',
                      'order' => 'ASC',
                      
                    );
                    query_posts( $args );
                    if (have_posts()) : while (have_posts()) : the_post(); 
                ?>
                  <li>
                  <div class="bottom-testimonials col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="testimonial-thumb"><?php echo get_the_post_thumbnail(); ?></div>
                    <div class="testimonial-content"><?php echo get_the_content(); ?></div>
                  </div>
                  </li>
                <?php
                endwhile;
                endif;
                wp_reset_query();
                ?>
              
              
              </ul>
            </div>
          </div>
        </div>





  <?php
}

add_shortcode( 'demandfluence-home-testimonials', 'testimonials_section' );




function clients_section() {
  ?>
  <!-- End Our courses titile -->
        <!-- Start Our courses content -->
        <div class="clients-holder col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="client_content">
              <ul class="clients_nav">
                
                   <?php
                  global $post;
                          $args = array(
                      'post_type' => 'clients',
                      'showposts' => -1,
                      'post_status' => 'publish',
                      'order' => 'ASC',
                      
                    );
                    query_posts( $args );
                    if (have_posts()) : while (have_posts()) : the_post(); 
                ?>
                  <li>
                  <div class="client-holder col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="client-thumb"><?php echo get_the_post_thumbnail(); ?></div>
                    <div class="client-content"><?php echo get_the_content(); ?></div>
                  </div>
                  </li>
                <?php
                endwhile;
                endif;
                wp_reset_query();
                ?>
              
              
              </ul>
            </div>
          </div>
        </div>
  <?php
}
add_shortcode( 'demandfluence-home-clients', 'clients_section' );




function mid_section_one() {
  ?>
  
        <div class="mid-content-holder col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
          <?php
            global $post;
              $args = array(
              'post_type' => 'page',
              'page_id' => '62',
            );
              query_posts( $args );
              if (have_posts()) : while (have_posts()) : the_post(); 
            ?>

            <h3><?php echo get_the_title(); ?></h3>
              <div class="mid-content"><?php echo get_the_content(); ?></div>
              <div class="mid-content-read-more"><p><a title="mid-read-more" href="<?php echo get_permalink(); ?>">Read More</a></p></div>
          <?php
            endwhile;
            endif;
            wp_reset_query();
          ?>
            
        </div>

<?php
}

add_shortcode( 'mid-section-one-content', 'mid_section_one' );

function mid_section_two() {
  ?>
  <div class="bottom-callouts col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="row">
    <div class="marketing-qualified-leads bottom-callout col-lg-4 col-md-4 col-sm-4 col-xs-12  wow fadeInUp">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '65',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <div class="post-thumbnail-holder"><?php echo get_the_post_thumbnail(); ?></div>
      <h4><?php echo get_the_title(); ?></h4>
      <?php $trimmed_content =  wp_trim_words( get_the_content(), 80, '...' ); ?>
      <div class="bottom-callout-content">
        <?php echo $trimmed_content; ?>
          <p class="read-more-holder"><a class="read-more" title="mid-read-more" href="<?php echo get_permalink(); ?>">Read More</a></p>
      </div>
      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="sales-qualified-leads bottom-callout col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInUp">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '67',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <div class="post-thumbnail-holder"><?php echo get_the_post_thumbnail(); ?></div>
      <h4><?php echo get_the_title(); ?></h4>
      <div class="bottom-callout-content">
        <?php echo $trimmed_content; ?>
          <p class="read-more-holder"><a class="read-more" title="mid-read-more" href="<?php echo get_permalink(); ?>">Read More</a></p>
      </div>
      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="consulting bottom-callout col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInUp">
      <?php
        global $post;
                $args = array(
            'post_type' => 'page',
            'page_id' => '69',
          );
          query_posts( $args );
          if (have_posts()) : while (have_posts()) : the_post(); 
      ?>


      <div class="post-thumbnail-holder"><?php echo get_the_post_thumbnail(); ?></div>
      <h4><?php echo get_the_title(); ?></h4>
      <div class="bottom-callout-content">
        <?php echo $trimmed_content; ?>
          <p class="read-more-holder"><a class="read-more" title="mid-read-more" href="<?php echo get_permalink(); ?>">Read More</a></p>
      </div>

      <?php
      endwhile;
      endif;
      wp_reset_query();
      ?>
    </div>

    
  </div>
</div>

<?php

}

add_shortcode( 'mid-section-two-content', 'mid_section_two' );